import { put, takeLatest,takeEvery, all, delay } from 'redux-saga/effects';
import {ConstantClass} from './ConstantClass'
const num = 1

function* fetchNews() {
  const json = yield fetch("http://newsapi.org/v2/everything?q=bitcoin&from=2020-07-12&sortBy=publishedAt&apiKey=3c2300055a4b413eb90eeff294e2eda8")
        .then(response => response.json(), );   
        try{
            console.log("came here")
            yield put({ type: "NEWS_RECEIVED", json: json.articles });
        } 
        catch{
console.log("Error")

        }
  
}
function* incermentFunc(){
    yield delay(1000)
    ConstantClass.num += 1
    yield put({ type: 'INCREMENT', value:ConstantClass.num})
}
function* actionWatcher() {
     yield takeLatest('GET_NEWS', fetchNews)
}
function* watchIncrementAsync() {
    yield takeEvery('INCREMENT_ASYNC', incermentFunc)
  }
export default function* rootSaga() {
   yield all([
   actionWatcher(),
   watchIncrementAsync()
   ]);
}