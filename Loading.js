import React from 'react';
import { connect } from 'react-redux'
import { View,ActivityIndicator } from 'react-native';


let Loading = ({ loading }) => (
loading ?
<View style={{ textAlign: 'center' }}>
<ActivityIndicator style = {{justifyContent:"center",alignItems:"center"}}/>
</View> :
<View/>
);
const mapStateToProps = (state) => ({loading: state.loading})
Loading = connect(mapStateToProps,null)(Loading)
export default Loading;