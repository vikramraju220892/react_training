import React from 'react';
import { connect } from 'react-redux';
import { incerementAct } from './action';
import{Button} from 'react-native'

let IncrementButton=({incerementAct})=>(
    //    <button onClick={getNews}>Press to see news</button>
    <Button  title = "Increment" onPress = {incerementAct}
    />
    )
    const mapDispatchIncToProps = {
        incerementAct: incerementAct,
    };
    IncrementButton = connect(null,mapDispatchIncToProps)(IncrementButton);
    export default IncrementButton;
