import React from 'react';
import { connect } from 'react-redux';
import { getNews } from './action';
import{Button} from 'react-native'
let NewButton=({getNews})=>(
//    <button onClick={getNews}>Press to see news</button>
<Button  title = "Press to see news" onPress = {getNews}
/>
)
const mapDispatchToProps = {
     getNews: getNews,
};
NewButton = connect(null,mapDispatchToProps)(NewButton);
export default NewButton;


