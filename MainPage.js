
import {Text,View} from 'react-native'
import React,{Component} from 'react'
import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from './reducer';
import rootSaga from './sagas';
import NewButton from './Button';
import NewsItem from './NewsItem'
import Loading from './Loading'
import IncrementButton from './IncrementButton'
import IncrementItem from './NewIncrementScreen'
import {ConstantClass} from './ConstantClass'
class MainPage extends Component {
    state = {  }
    
    render() { 
    const sagaMiddleware = createSagaMiddleware();

const store = createStore(
   reducer,
   applyMiddleware(sagaMiddleware),
);
sagaMiddleware.run(rootSaga);
        console.log('Tets de')
        return (
            <Provider store={store}><View>
            <NewButton />
            <IncrementButton/>
            <Loading />
            <NewsItem />
            <IncrementItem/>
        <Text> {ConstantClass.num}</Text>
            </View>
            </Provider>);
    }
}
 
export default MainPage;