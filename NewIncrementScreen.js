import React from 'react'
import {Text,View} from 'react-native'
import { connect } from 'react-redux'
let IncrementItem = ({ value}) => (
    value ?
  <View style ={{alignItems:'center'}}>
    <Text>{value}</Text>
  </View>
 :
null
);
const mapStateToProps = (state) => ({
value: state.value
})
IncrementItem = connect(mapStateToProps,null)(IncrementItem)
export default IncrementItem;
