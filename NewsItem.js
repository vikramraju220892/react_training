import React from 'react';
import {Text,View, Image} from 'react-native'
import { connect } from 'react-redux'
const imgStyle = {
  height: 80,
  width: 80,
  borderRadius: 5
};
const articleStyle = {
width: '80%',
margin: 10,
color: 'olive'
}
let NewsItem = ({ article,isNews }) => (
    isNews ?
<View style={articleStyle} >
  <View>
    <Text>{article.title}</Text>
   <Image style = {imgStyle} source = {{uri : article.urlToImage}} />
  </View>
</View> :
null
);
const mapStateToProps = (state) => ({
article: state.news,
isNews:state.isNews
})
NewsItem = connect(mapStateToProps,null)(NewsItem)
export default NewsItem;